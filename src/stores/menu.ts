import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";

export const useMenuStore = defineStore("menu", () => {
  const menu = ref<Menu[]>([
    {
      id: 0,
      name: "Coffee",
      price: 35,
      category: "Drinks",
      img: "https://www.tastingtable.com/img/gallery/coffee-brands-ranked-from-worst-to-best/l-intro-1645231221.jpg",
    },
    {
      id: 0,
      name: "Milk",
      price: 40,
      category: "Drinks",
      img: "https://images.immediate.co.uk/production/volatile/sites/30/2020/02/Glass-and-bottle-of-milk-fe0997a.jpg",
    },
    {
      id: 0,
      name: "Crossoint",
      price: 39,
      category: "Dessert",
      img: "https://cdn.britannica.com/65/235965-050-A5D740E2/Croissants-jar-of-jam.jpg",
    },
    {
      id: 0,
      name: "Spaghetti",
      price: 60,
      category: "Food",
      img: "https://veganwithgusto.com/wp-content/uploads/2021/05/speedy-spaghetti-arrabbiata-featured-e1649949762421.jpg ",
    },
  ]);

  return { menu };
});
