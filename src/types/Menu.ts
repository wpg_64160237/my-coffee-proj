export default interface Menu {
  id: number;
  name: string;
  price: number;
  category: string;
  img: string;
}
